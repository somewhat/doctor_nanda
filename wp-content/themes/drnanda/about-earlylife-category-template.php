<?php
/*
 * Template Name: About Early Life Category Template
 * Template Post Type: post
 */
  
get_header(); ?>

                	<?php $currentPostId =  get_the_ID();
						global $post;
						$postcat = get_the_category( $post->ID );
						$currentCatId = $postcat[0]->term_id;
						$currentCatName = $postcat[0]->cat_name;
						$currentCatName = str_replace(" ","-",$currentCatName);
					?>
					<ul class="single-post-tab-list">
					<?php if(have_posts()) : ?>
                     <?php while(have_posts()) : the_post(); ?>
						<?php
						$current_date ="";
						$count_posts = wp_count_posts();
						$nextpost = 0;
						$published_posts = $count_posts->publish;
						$myposts = get_posts(array('category'=>$currentCatId,'posts_per_page'=>'1000','meta_key'=> 'page_order','orderby'=>'meta_value','order'=> 'ASC')); 
						foreach($myposts as $post) :
						$nextpost++;
						setup_postdata($post);
						$date = get_the_date("F Y"); 
						$id = get_the_ID();
						$newsurl = get_site_url()."/news/?newsid=".$id;
						// echo $id;
						if($current_date!=$date): 
						if($nextpost>1): ?> 

						<?php endif; ?> 

						<?php $current_date=$date;
						endif; ?>

						<li class="<?php if($currentPostId == $id){echo 'active';} ?>">
						<a href="<?php the_permalink(); ?>">
							<?php $title = get_the_title(); ?>
							<div class="newsHeading"><?php echo $title; ?></div>
						</a>
						</li>
						<?php endforeach; wp_reset_postdata(); ?>

                     <?php endwhile; ?>
                    <?php endif; ?>
					</ul>
					<div class="single-post-tab-content" id="tabcontent">
						<h1><?php the_title(); ?></h1>
	                    <?php $query = get_post(get_the_ID());
						$content = apply_filters('the_content', $query->post_content);
						echo $content; ?>
					</div>

    <script>
	(function($) {
		var currentCatClass = "<?php echo $currentCatName; ?>";
		//$(".field-of-expertise-menu").addClass('current-menu-item page_item current_page_item');
		$('.site-content').css({'padding-left':0});
		$("."+currentCatClass+"-menu").addClass('current-menu-item page_item current_page_item');
		console.log("Template about earlylife");

	})( jQuery );
	
	</script>
<?php get_footer(); ?>