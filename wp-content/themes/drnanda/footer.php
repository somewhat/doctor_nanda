<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div></div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						 ) );
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>

			<div class="site-info">
				<div class="tnc-link">
					<!--<a href="terms-conditions">TERMS & CONDITIONS</a> &nbsp;&nbsp;| &nbsp;&nbsp;<a href="refund-cancellation">REFUND & CANCELLATION</a>-->
				</div>
				<p class="copyright-info">© 2018. ALL RIGHTS RESERVED.</p>
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
				<!-- <span class="site-title"><a href="<?php //echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a></span> -->
				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}
				?>
				<!-- <a href="<?php //echo esc_url( __( 'https://wordpress.org/', 'twentysixteen' ) ); ?>" class="imprint">
					<?php //printf( __( 'Proudly powered by %s', 'twentysixteen' ), 'WordPress' ); ?>
				</a> -->
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<div class="iframe-popup">
	<div class="iframe-popup-video">
	</div>
</div>

<div class="image-popup-wrap">
    <div class="image-popup-close"></div>
    <div class="image-popup-inner-wrap">
    	<img src="" id="imagesrc">
    </div>
</div>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/form.js" ></script>
<!--<script type="text/javascript" src="<?php //bloginfo('template_directory'); ?>/js/jquery.scrollbar.min.js"></script>-->
<script type="text/javascript">
	(function ($){

		// main section height 
		var winHeight = $(window).height();
		var winHeightHeader = $('header').height(); // Header Height
		var winHeightFooter = $('footer').height(); // Footer Height
		var winHeaderFooterHeight = winHeightHeader + winHeightFooter; // Header+Footer Height
		var siteContentHeight = winHeight - (winHeightHeader + 56); //Site-Content page height
		// console.log(winHeightHeader);
		// console.log(winHeight);
		// console.log(siteContentHeight);		
		// console.log(winHeightFooter);

		// page height
		if($(window).width() >=1025){
			$('.site-content-wrap').height(winHeight - (winHeightHeader + 56));
			$('.single-post-tab-content').css({'min-height':(winHeight - (winHeightHeader + 56))});
			//$('.single-post-tab-content-inner').height(winHeight - (winHeightHeader + 445));
			//$('.single-post-tab-content-inner').addClass('scrollbar-inner');
			jQuery('.scrollbar-inner').scrollbar();
		}
		
		//Contact page height
		var contactContentHeight = $('.contact-wrap').height(); //Contact content height
		var contactCalHeight = ((siteContentHeight - contactContentHeight) / 2) - 15;
		$('.contact-wrap').css('padding-top', contactCalHeight);

		//Book an appoinment page height
		var publicationContentHeight = $('.appointment-book-wrap').height(); //Contact content height
		var publicationCalHeight = ((siteContentHeight - publicationContentHeight) / 2) - 10;
		$('.appointment-book-wrap').css('padding-top', publicationCalHeight);
		
		
		$('.home-section1-left').height(winHeight - (winHeightHeader + 66));
		$('.about-category-left').height(winHeight - (winHeightHeader + 56));
		$('.about-category-left img').height(winHeight - (winHeightHeader + 56));
		//$('.video-slider-main-left').height(winHeight - (winHeightHeader + 45 ));

		
		//$('.iframe-url').each(function(index, element) {
		$( document ).on( "click", ".iframe-url", function() {
			
           // $(this).click(function(){
				$(".iframe-popup-inner-wrap").append('<iframe id="video-iframe" src="'+$(this).attr('video-url')+'?rel=0&autoplay=1" frameborder="0" allowfullscreen="0" ></iframe>');
				$('.iframe-popup-wrap').show().animate({
		 			opacity: 1,
		 		}, 1000, function() {
		 		// Animation complete.
		 		});
			//});
        });
		$('.iframe-popup-close').click(function(){
			$('.iframe-popup-wrap').animate({
				opacity: 0,
			}, 1000, function() {
			// Animation complete.
				$('.iframe-popup-wrap').hide();
				$(".iframe-popup-inner-wrap").html('');
			});
		});
		$('.main-navigation li')
		.mouseenter(function() {
			if($(this).hasClass('menu-item-has-children')){
				if(!$(this).hasClass('current-menu-ancestor')){
					$('.main-navigation li.current-menu-ancestor > ul').css({'left':'-999em'});
				}
			}
		})
  		.mouseleave(function() {
			if($(this).hasClass('menu-item-has-children')){
				if(!$(this).hasClass('current-menu-ancestor')){
					$('.main-navigation li.current-menu-ancestor > ul').css({'left':'20px'});
				}
			}
  		});


  		$('.surgery-sub-head').click(function( event ) {
			if($(this).hasClass('active')){
				$( this ).removeClass('active');
				$( this ).next('.surgery-sub-desc').slideUp();
			}else {
				$( this ).addClass('active');
				$( this ).next('.surgery-sub-desc').slideDown();
			}
		});

  		//image zoom
  		// $('.surgery-sub-desc p img').click(function(){
  		// 	//$(this).animate({width: "100%"});
  		// 	$(this).toggleClass('img-popup');
  		// });

  		$('#mixedSlider').multislider({
		    duration: 1000,
			interval: false
		});
		$('#mixedSlider_2').multislider({
		    duration: 1000,
			interval: false
		});

		$('.surgery-sub-desc img').click(function(){
		  var image_src = $(this).attr('src');
		  var image_src_w = $(this).width();
		  var image_src_h = $(this).height();
		  $('#imagesrc').height(image_src_h);
		  $('#imagesrc').width(image_src_w);
		  //
		  //alert($(window).height() +'      '+image_src_h);
		  if ($(window).height() < image_src_h ) {
		  	$('#imagesrc').height($(window).height()-20);
		  	image_src_h = $('#imagesrc').height();
		  	image_src_w = $('#imagesrc').width();
		  }
		  var image_src_width = image_src_w / 2;
		  var image_src_height = image_src_h / 2;
		  // alert(image_src);
		  // console.log(image_src);
		  // console.log(image_src_w);
		  // console.log(image_src_h);
		  // console.log(image_src_width);
		  // console.log(image_src_height);
		  $('#imagesrc').attr("src",image_src);
		  $('.image-popup-wrap').css("display", "block");
		  $('.image-popup-wrap').css("opacity", 1);
		  $('.image-popup-inner-wrap').css("margin-top", -image_src_height);
		  $('.image-popup-inner-wrap').css("margin-left", -image_src_width);
		  
		  // if (image_src_h > 1000) {
		  // 	$('#imagesrc').css("height", 700 );
		  // 	$('.image-popup-inner-wrap').css("margin-top", -350);
		  // }
		  // else {
		  // 	$('.image-popup-inner-wrap').css("margin-top", -image_src_height);
		  // 	$('.image-popup-inner-wrap').css("margin-left", -image_src_width);
		  // }
		});
		$('.image-popup-close').click(function(){
			$('.image-popup-wrap').animate({
				opacity: 0,
			}, 1000, function() {
			// Animation complete.
				$('.image-popup-wrap').hide();
				//$(".image-popup-inner-wrap").html('');
			});
		});


	})(jQuery);
</script>
</body>
</html>
