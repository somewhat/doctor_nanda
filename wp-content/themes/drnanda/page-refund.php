<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Refund Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php 
		$refund_page_id = 330;
		$description = get_the_content();
		$header_image = get_site_url().'/'.get_post_meta($refund_page_id, 'header-image', true);
	?>
	<section class="refund-section1">
		<div class="desc">
			<?php /* Start the Loop */ ?>
				<?php while(have_posts()) : the_post(); ?>
				<?php the_content();?>
			<?php endwhile; ?>
		</div>
	</section>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			//get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<!-- <?php //get_sidebar( 'content-bottom' ); ?> -->

</div><!-- .content-area -->

<!-- <?php //get_sidebar(); ?> -->
<?php get_footer(); ?>
