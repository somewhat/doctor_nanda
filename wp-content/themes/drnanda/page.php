<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Home Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php 
		$home_page_id = 324;
		$description = get_the_content();
		$header_image = get_site_url().'/'.get_post_meta($home_page_id, 'header-image', true);
	?>
	<section class="home-section1">
		<!-- <div class="home-section1-left w-50">
			<?php 
				//if ( has_post_thumbnail() ) {			the_post_thumbnail();			} 
			?>
		</div> -->
		<div class="home-section1-right w-50">
			<div class="desc">
				<img src="http://creative.2adpro.com/doctor_nanda/wp-content/themes/drnanda/image/quotes-left.png">
				<p> <?php echo get_post_meta($home_page_id, 'description', true); ?> <img src="http://creative.2adpro.com/doctor_nanda/wp-content/themes/drnanda/image/quotes-right.png"></p>
			</div>
			<div class="">
				<div class="home-read-more-div mt50">
					<a href="about-doctor/early-life/" class="home-read-more"><?php echo get_post_meta($home_page_id, 'link_title_1',true); ?></a>
				</div>
				<div class="home-read-more-div ml20">
					<a href="contact/" class="home-read-more"><?php echo get_post_meta($home_page_id, 'link_title_2',true); ?></a>
				</div>
			</div>
		</div>
	</section>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			//get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<!-- <?php //get_sidebar( 'content-bottom' ); ?> -->

</div><!-- .content-area -->

<!-- <?php //get_sidebar(); ?> -->
<?php get_footer(); ?>
