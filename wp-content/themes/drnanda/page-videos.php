<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Videos Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main " role="main">
		<section class="video-section1 clearfix">

			<?php if(have_posts()) : ?>
	        <?php while(have_posts()) : the_post(); ?>
	        <?php the_content(); ?>
	        <?php
	            $current_date ="";
	            $count_posts = wp_count_posts();
	            $nextpost = 0;
	            $published_posts = $count_posts->publish;
	            $myposts = get_posts(array('posts_per_page'=>$published_posts,'category'=>16,'meta_key'=> 'page_order','orderby'=>'meta_value_num','order'=> 'ASC'));
	            foreach($myposts as $post) :
			           $nextpost++;
			           setup_postdata($post);
			           $date = get_the_date("F Y");
			           $id = get_the_ID();
			          if($current_date!=$date):
			              if($nextpost>1):
			          ?> 
			          <?php endif; ?>
							<?php $current_date=$date;
			                 endif; ?>
			                <?php 
			                	$title = get_the_title();
			                	$description = get_the_content();
			                	$iframe_url = get_post_meta($id, 'iframe_url', true);
								$videoid = substr($iframe_url, strrpos($iframe_url, '/') + 1);
			                ?>
                            <?php

//set api url
$url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=$videoid&key=AIzaSyA5Ks3foHeHa3xc_LGJ__MCEnXAs1z_8dI";

//call api
$json = file_get_contents($url);
$json = json_decode($json);
$likecount = $json->items[0]->statistics->likeCount;

?>
                            
			                <div class="video-frame" videoid="<?php echo $videoid ?>" likecount="<?php echo $likecount; ?>">
                            	<div class="video-frame-inner" >
                                	<iframe height="200" src="<?php echo $iframe_url; ?>?showinfo=0" frameborder="0" allowfullscreen="" style="width:100%;"></iframe>              <div class="iframe-url" video-url="<?php echo $iframe_url; ?>"></div>
                                    <div class="video-title"><p><?php echo $title; ?></p><span>- Dr. Nanda Rajaneesh</span></div>
                                </div>
							</div>
			            <?php endforeach; wp_reset_postdata(); ?>
			                <?php endwhile; ?>
			            <?php endif; ?>
		</section>

		


		
	</main><!-- .site-main -->



</div><!-- .content-area -->

<div class="iframe-popup-wrap">
        	<div class="iframe-popup-close"></div>
        	<div class="iframe-popup-inner-wrap">
			<!--<iframe id="video-iframe" src="https://www.youtube.com/embed/89c6hYgEUnY?autoplay=1" frameborder="0" allowfullscreen="0" ></iframe>-->
            </div>
		</div>


<?php get_footer(); ?>
<script>
(function ($){
	$( window ).load(function() {
		var $divs = $("div.video-frame");
	

		var numericallyOrderedDivs = $divs.sort(function (a, b) {
			return parseInt($(a).attr('likecount')) < parseInt($(b).attr('likecount'));
		});
		$(".video-section1").html(numericallyOrderedDivs);

 	});	
})(jQuery);	
</script>