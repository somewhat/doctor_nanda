<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Awards & Achievement Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php 
		$home_page_id = 744;
		$description = get_the_content();
		$header_image = get_site_url().'/'.get_post_meta($home_page_id, 'header-image', true);
	?>

	

	<!-- <section class="video-slider-main">
		<div class="video-slider-main-left">
			<div class="video-frame">
            	<iframe height="400" src="https://www.youtube.com/embed/3tVeAwE3yqY?showinfo=0" frameborder="0" allowfullscreen="" style="width:100%;"></iframe>
                <div class="video-slider-title">Lorem Ipsum copy in various charsets and languages for layouts.</div>
			</div>
		</div>
		<div class="video-slider-main-right">Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.Lorem Ipsum copy in various charsets and languages for layouts.</div>
	</section> -->


	<section class="video-slider-main clearfix hidden">
		<div class="video-slider-main-left"  id="mixedSlider">
			<div class="MS-content">
			<?php if(have_posts()) : ?>
		      <?php while(have_posts()) : the_post(); ?>
		      <?php the_content(); ?>
		      <?php
		        $current_date ="";
		        $count_posts = wp_count_posts();
		        $nextpost = 0;
		        $published_posts = $count_posts->publish;
		        $myposts = get_posts(array('posts_per_page'=>$published_posts,'category'=>20,'meta_key'=> 'page_order','orderby'=>'meta_value_num','order'=> 'ASC'));
		        foreach($myposts as $post) :
		           $nextpost++;
		           setup_postdata($post);
		           $date = get_the_date("F Y");
		           $id = get_the_ID();
		          if($current_date!=$date):
		              if($nextpost>1):
		          ?> 
		          <?php endif; ?>
						<?php $current_date=$date;
			                 endif; ?>					          	
			                <?php 
			                	$title = get_the_title();
			                	$description = get_the_content();
			                	// $post_second_image = get_post_meta($id, 'post_second_image', true);
			                	// $pdf_link = get_post_meta($id, 'pdf_link', true);
			                ?>
			                <div class="video-frame item">
			                	<?php echo $description; ?>
                				<div class="video-slider-title"><?php echo $title; ?></div>
			                </div>
			         	
			            <?php endforeach; wp_reset_postdata(); ?>
			                <?php endwhile; ?>
			            <?php endif; ?> 
	    	</div>
	    	<div class="MS-controls">
			    <button class="MS-left"><img src="<?php echo get_site_url(); ?>/wp-content/themes/drnanda/image/left-arrow.png"></button>
			    <button class="MS-right"><img src="<?php echo get_site_url(); ?>/wp-content/themes/drnanda/image/right-arrow.png"></button>
			</div>
	    </div>
	    
	    

		<!-- Right side ssection -->
		<div class="video-slider-main-right clearfix" id="mixedSlider_2">
			<div class="MS-content">
			<?php if(have_posts()) : ?>
		      <?php while(have_posts()) : the_post(); ?>
		      <?php the_content(); ?>
		      <?php
		        $current_date ="";
		        $count_posts = wp_count_posts();
		        $nextpost = 0;
		        $published_posts = $count_posts->publish;
		        $myposts = get_posts(array('posts_per_page'=>$published_posts,'category'=>20,'meta_key'=> 'page_order','orderby'=>'meta_value_num','order'=> 'ASC'));
		        foreach($myposts as $post) :
		           $nextpost++;
		           setup_postdata($post);
		           $date = get_the_date("F Y");
		           $id = get_the_ID();
		          if($current_date!=$date):
		              if($nextpost>1):
		          ?> 
		          <?php endif; ?>
						<?php $current_date=$date;
			                 endif; ?>					          	
			                <?php 
			                	$title = get_the_title();
			                	$description = get_the_content();
			                	// $post_second_image = get_post_meta($id, 'post_second_image', true);
			                	// $pdf_link = get_post_meta($id, 'pdf_link', true);
			                ?>
			                <div class="video-frame item">
                				<div class="video-slider-video"><?php echo $description; ?></div>
			                </div>
			         	
			            <?php endforeach; wp_reset_postdata(); ?>
			                <?php endwhile; ?>
			            <?php endif; ?> 
	    	</div>
	    	<!-- <div class="MS-controls">
			    <button class="MS-left"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/left-arrow.png"></button>
			    <button class="MS-right"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/right-arrow.png"></button>
			</div> -->

		</div>
	</section>

	<section>
    	<div style="display:none;" class="html5gallery" data-skin="vertical" data-width="100%" data-height="475">
	       <!-- Add images to Gallery -->
	       <a href="http://creative.2adpro.com/doctor_nanda/wp-content/uploads/2018/08/file1.jpeg" class="gallery1"><img src="<?php echo get_site_url(); ?>/wp-content/themes/drnanda/image/video-thumbnail.png" alt="What does anastrozole & chemotherapy do for breast cancer?"></a>

	       <!-- <a href="http://creative.2adpro.com/doctor_nanda/wp-content/uploads/2018/08/file7.jpeg" class="gallery2"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/video-thumbnail.png" alt="Why cancer is becoming so common nowadays?"></a> -->

	       <!-- Add videos to Gallery -->
	       <!-- <a href="http://creative.2adpro.com/doctor_nanda/wp-content/uploads/2018/08/file8.jpeg" class="gallery3"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/video-thumbnail.png" alt="Can men get breast cancer & what are chances of it?"></a> -->
	       
	       <!-- Add Youtube video to Gallery -->
	       <!-- <a href="http://creative.2adpro.com/doctor_nanda/wp-content/uploads/2018/08/file9.jpeg" class="gallery4"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/video-thumbnail.png" alt="What is the meaning & survival rate of triple negative breast cancer?"></a> -->
	       
	       <!-- Add Vimeo video to Gallery -->
	       <!-- <a href="http://creative.2adpro.com/doctor_nanda/wp-content/uploads/2018/08/file2.jpeg" class="gallery5"><img src="<?php //echo get_site_url(); ?>/wp-content/themes/drnanda/image/video-thumbnail.png" alt="Can pesticide sprays across community lead to cancer?"></a> -->
    	</div>
	</section>
	<section class="awards-section2">
		<div class="">
			<?php /* Start the Loop */ ?>
			<?php while(have_posts()) : the_post(); ?>
			<?php the_content();?>
			<?php endwhile; ?>
		</div>
	</section>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			//get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<!-- <?php //get_sidebar( 'content-bottom' ); ?> -->

</div><!-- .content-area -->

<!-- <?php //get_sidebar(); ?> -->
<?php get_footer(); ?>
