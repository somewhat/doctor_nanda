<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Early Life Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php 
		$early_life_page_id = 370;
		$header_image = get_site_url().'/'.get_post_meta($early_life_page_id, 'header-image', true);
	?>
	<!-- <section class="early_life-section1 about-category">
		<div class="desc">
			<?php /* Start the Loop */ ?>
				<?php //while(have_posts()) : the_post(); ?>
				<?php //the_content();?>
			<?php //endwhile; ?>
		</div>
	</section> -->

	<section class="early_life-section1 about-category">
		<div class="about-category-left w-50">
			<?php 
				if ( has_post_thumbnail() ) {			the_post_thumbnail();			} 
			?>
		</div>
		<div class="about-category-right w-50">
			<div class="desc">
				<?php /* Start the Loop */ ?>
					<?php while(have_posts()) : the_post(); ?>
					<?php the_content();?>
				<?php endwhile; ?>
			</div>
		</div>
	</section>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			//get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->


</div><!-- .content-area -->

<?php get_footer(); ?>
