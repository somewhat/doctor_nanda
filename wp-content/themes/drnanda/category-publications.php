<?php
get_header(); ?>

					<ul class="single-post-tab-list">
					<?php 
					$category = get_queried_object();
					$currentCatId = $category->term_id;

						$myposts = get_posts(array('category'=>$currentCatId,'post_type' =>  'post', 'meta_key'=> 'page_order','orderby'=>'meta_value','order'=> 'ASC')); 
						//echo "<pre>";print_r($myposts);echo "</pre>";
						$i=1;
						$first_post_id = "";
						foreach($myposts as $post) :
						$nextpost++;
						setup_postdata($post);
						$date = get_the_date("F Y"); 
						$id = get_the_ID();
						if($first_post_id===""){
						$first_post_id = $id; 
						}
						$newsurl = get_site_url()."/news/?newsid=".$id;
						?>

						<li class="<?php if($i == 1){echo 'active';} ?>">
						<!-- <a href="<?php //the_permalink(); ?>"> -->
							<?php $title = get_the_title(); ?>
							<div class="newsHeading"><?php echo $title; ?></div>
						<!-- </a> -->
						</li>
						<?php 
						$i++;
						endforeach; wp_reset_postdata(); ?>

					</ul>
					<div class="single-post-tab-content">
					<?php 
						    $query = get_post($first_post_id); 
							$content = apply_filters('the_content', $query->post_content);
					?>
						<h1><?php echo $query->post_title; ?></h1>
						<div class="single-post-tab-content-inner">
	                    	<!-- <?php 

							//echo $content; ?> -->
						</div>
					</div>

    <script>
	(function($) {
		var currentCatClass = "<?php echo $currentCatName; ?>";
		$(".field-of-expertise-menu").addClass('current-menu-item page_item current_page_item');
		$('.site-content').css({'padding-left':0});
		$("."+currentCatClass+"-menu").addClass('current-menu-item page_item current_page_item');
		
	})( jQuery );
	
	</script>
<?php get_footer(); ?>