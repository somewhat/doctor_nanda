<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Experience Page
 */

get_header(); ?>

					<ul class="single-post-tab-list">
					<?php 
					$category = get_queried_object();
					$currentCatId = $category->term_id;

						$myposts = get_posts(array('category'=>$currentCatId,'post_type' =>  'post', 'meta_key'=> 'page_order','orderby'=>'meta_value','order'=> 'ASC'));
						//echo "<pre>";print_r($myposts);echo "</pre>";
						$i=1;
						$first_id = "";
						//echo "<pre>"; print_r($myposts); echo "</pre>";
						foreach($myposts as $post) :
						$nextpost++;
						setup_postdata($post);
						$date = get_the_date("F Y"); 
						$id = get_the_ID();
						$newsurl = get_site_url()."/news/?newsid=".$id;
						if($first_id=="")
						{
							$first_id = $id;

						}
						?>

						<li class="<?php if($i == 1){echo 'active';} ?>">
						<a href="<?php the_permalink(); ?>">
							<?php $title = get_the_title(); ?>
							<div class="newsHeading"><?php echo $title; ?></div>
						</a>
						</li>
						<?php 
						$i++;
						endforeach; wp_reset_postdata(); ?>

					</ul>
					<div class="single-post-tab-content">
						<?php $query = get_post($first_id); ?>
						<h1><?php echo $query->post_title; ?></h1>
						<div class="single-post-tab-content-inner">
	                    	<?php  
							$content = apply_filters('the_content', $query->post_content);
							echo $content; ?>
						</div>
					</div>

    <script>
	(function($) {
		var currentCatClass = "<?php echo $currentCatName; ?>";
		// $(".field-of-expertise-menu").addClass('current-menu-item page_item current_page_item');
		$('.site-content').css({'padding-left':0});
		$("."+currentCatClass+"-menu").addClass('current-menu-item page_item current_page_item');

	})( jQuery );
	
	</script>
<?php get_footer(); ?>
