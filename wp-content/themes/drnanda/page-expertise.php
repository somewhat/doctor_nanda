<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 /*
    Template Name: Field of Expertise Page
 */

get_header(); ?>


	<?php $curid = get_the_ID(); $fetch_id = get_post_meta($curid, 'fetch-category', true); ?>
	<ul class="single-post-tab-list">
	<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>

	<?php
	$current_date ="";
	$count_posts = wp_count_posts();
	$nextpost = 0;
	$published_posts = $count_posts->publish;
	$myposts = get_posts(array('category'=>$fetch_id,'posts_per_page'=>'1000','meta_key'=> 'page_order','orderby'=>'meta_value','order'=> 'ASC')); 
	foreach($myposts as $post) :
	$nextpost++;
	setup_postdata($post);
	$date = get_the_date("F Y"); 
	$id = get_the_ID();
	$newsurl = get_site_url()."/news/?newsid=".$id;
	// echo $id;
	if($current_date!=$date): 
	if($nextpost>1): ?> 

	<?php endif; ?> 

	<?php $current_date=$date;
	endif; ?>


	<li class="<?php if($nextpost == 1){echo 'active'; $curPostId = $id; } ?>">
		<a href="<?php the_permalink(); ?>">
			<?php $title = get_the_title(); ?>
			<div class="newsHeading"><?php echo $title; ?></div>
		</a>
	</li>
	<?php endforeach; wp_reset_postdata(); ?>

	<?php endwhile; ?>
	<?php endif; ?>
	</ul>
	<div class="single-post-tab-content">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>book-an-appointment" class="book-appointment-link">BOOK AN APPOINTMENT</a>
		<h1><?php echo get_the_title( $curPostId ); ?> </h1>
		<?php $query = get_post($curPostId); 
		$content = apply_filters('the_content', $query->post_content);
		echo $content; ?>
	</div>
<script>
	(function($) {
		$('.site-content').css({'padding-left':0});
	})( jQuery );
	
	</script>
<?php get_footer(); ?>
